<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if ($arParams["ID_IBLOCK_REL"]) {
    if(!empty($arResult["CANONICAL"])){
    $APPLICATION->SetPageProperty("canonical", '<link rel="canonical" href="'.$arResult["CANONICAL"].'">');
    }
}
