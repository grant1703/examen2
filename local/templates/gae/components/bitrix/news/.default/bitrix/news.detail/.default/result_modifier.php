<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if ($arParams["ID_IBLOCK_REL"]) {
    $arOrder = array();
    $arFilter = array("IBLOCK_ID" => $arParams["ID_IBLOCK_REL"], "PROPERTY_NEW" => $arResult["ID"]);
    $arSelectFields = array("IBLOCK_ID", "ID", "NAME");
    $obCanonical = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
    if ($elementObCanonical = $obCanonical->GetNext()){
        $arResult["CANONICAL"] = $elementObCanonical["NAME"];
    }

    $obComponent = $this->getComponent();
    $obComponent->SetResultCacheKeys(array("CANONICAL"));
}

