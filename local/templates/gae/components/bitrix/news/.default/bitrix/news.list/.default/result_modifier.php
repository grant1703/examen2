<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


if ($arParams["SPECIALDATE"] == "Y") {
    $arFirstItems = reset($arResult["ITEMS"]);
    $arResult["FIRST_ITEMS_DATE"] = $arFirstItems["ACTIVE_FROM"];

    $obComponent = $this->getComponent();
    $obComponent->SetResultCacheKeys(array("FIRST_ITEMS_DATE"));
}

