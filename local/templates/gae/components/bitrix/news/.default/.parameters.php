<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
    "SPECIALDATE" => Array(
        "NAME" => GetMessage("SPECIALDATE"),
        "TYPE" => "CHECKBOX",
        "DEFAULT" => "N",
    ),
    "ID_IBLOCK_REL" => Array(
        "NAME" => GetMessage("ID_IBLOCK_REL"),
        "TYPE" => "STRING",
        "DEFAULT" => "",
    ),
);
?>