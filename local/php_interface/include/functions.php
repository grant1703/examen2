<?php

function dmp($var, $die)
{
    echo '<pre style=\'font-family: Verdana; white-space: pre-wrap; position: relative; z-index: 1000;\'>';
    var_dump($var);
    echo '</pre>';
    if ($die) {
        die();
    }
}