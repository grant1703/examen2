<?php
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$eventManager = \Bitrix\Main\EventManager::GetInstance();

$onBeforeIBlockElementUpdateHandler = $eventManager->AddEventHandler(
    "iblock",
    "OnBeforeIBlockElementUpdate",
    array('EventHanldlers', 'OnBeforeIBlockElementUpdate')
);

class EventHanldlers
{
    function OnBeforeIBlockElementUpdate (&$arFields) {
        if ($arFields['IBLOCK_ID'] == 2 && $arFields['ACTIVE'] == 'N') {
            $arSelect = Array("ID", "SHOW_COUNTER", "ACTIVE");
            $arFilter = Array("IBLOCK_ID"=>$arFields['IBLOCK_ID'], "ID"=>$arFields['ID']);
            $obElement = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
            $arElement = $obElement->Fetch();

            if ($arElement['SHOW_COUNTER'] > 2 && $arElement['ACTIVE'] == 'Y') {
                global $APPLICATION;
                $count = ['#COUNT#' => $arElement['SHOW_COUNTER']];
                $APPLICATION->throwException(Loc::getMessage('ELEMENT_EDITING_NOT_AVAILABLE', $count));
                return false;
            }
        }
    }
}

